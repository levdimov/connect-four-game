﻿using System;

namespace ConnectFourAI
{
	public class AiPlayer
	{
		/// <summary>
		/// Шаги, просчитываемые ИИ вперёд.
		/// </summary>
		private const int MaxDepth = 6;

		/// <summary>
		/// Значение допустимого отклонения для сравнения чисел с плавающей точкой
		/// </summary>
		public const double Tolerance = 0.00000001;

		/// <summary>
		/// Очки, выдаваемые при выйгрышном состоянии поля для текущего игрока.
		/// </summary>
		private const double WinOutcome = 1d;
		
		/// <summary>
		/// Очки, выдаваемые при проигрышном состоянии поля для текущего игрока.
		/// </summary>
		private const double LoseOutcome = -1d;

		private readonly Board gameBoard;

		public AiPlayer(Board currentGameBoard)
		{
			gameBoard = currentGameBoard;
		}

		/// <summary>
		/// Выполняет ход 
		/// </summary>
		/// <returns>Номер колонки поля, куда был выполнен ход</returns>
		public int MakeTurn()
		{	
			var maxValue = double.MinValue;
			var move = -1;

			for (var column = 0; column < gameBoard.Width; column++)
			{
				if (!gameBoard.IsValidMove(column)) continue;

				var columnValue = GetColumnDropValue(column);
				var value = MoveValue(column) + columnValue;
				
				if (value <= maxValue && move != -1) continue;
				
				maxValue = value;
				move = column;
			}

			gameBoard.MakeAiMove(move);
			return move;
		}

		/// <summary>
		/// Вычисление значимости хода в столбец
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>Значимость хода</returns>
		public double MoveValue(int column)
		{
			gameBoard.MakeAiMove(column);
			var value = AlphaBeta(MaxDepth, double.MinValue, double.MaxValue, false);
			gameBoard.UndoAiMove(column);
			return value;
		}

		/// <summary>
		/// Альфа-бета отсечение
		/// </summary>
		/// <param name="depth">Уровни глубины, которые необходимо проверить</param>
		/// <param name="alpha">Параметр альфа</param>
		/// <param name="beta">Параметр бета</param>
		/// <param name="maxPlayer">Тип игрока</param>
		/// <returns>Значимость хода</returns>
		public double AlphaBeta(int depth, double alpha, double beta, bool maxPlayer)
		{
			var gameHasWinner = (new BoardLineChecker(gameBoard)).HasWinner();

			if (depth == 0 || gameHasWinner)
			{
				return EvaluateGameState(depth, gameHasWinner, maxPlayer);
			}

			if (maxPlayer)
			{
				for (var column = 0; column < gameBoard.Width; column++)
				{
					if (!gameBoard.IsValidMove(column)) continue;

					gameBoard.MakeAiMove(column);
					alpha = Math.Max(
						alpha,
						AlphaBeta(
							depth: depth - 1,
							alpha: alpha,
							beta: beta,
							maxPlayer: false));
					gameBoard.UndoAiMove(column);
					if (beta <= alpha)
						break;
				}
				return alpha;
			}
			else
			{
				for (var column = 0; column < gameBoard.Width; column++)
				{
					if (!gameBoard.IsValidMove(column)) continue;

					gameBoard.MakePlayerMove(column);
					beta = Math.Min(
						beta,
						AlphaBeta(
							depth: depth - 1,
							alpha: alpha,
							beta: beta,
							maxPlayer: true));
					gameBoard.UndoPlayerMove(column);
					if (beta <= alpha)
						break;
				}
				return beta;
			}
		}

		/// <summary>
		/// Получение оценки хода в столбец
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>Значение оценки</returns>
		private double GetColumnDropValue(int column)
		{
			var discCount = Math.Pow(gameBoard.DiscCount + 1, gameBoard.DiscCount + 1);

			switch (column)
			{
				case 3:
					return 1.0 / discCount;
				case 2:
				case 4:
					return 0.5 / discCount;
				case 1:
				case 5:
					return 0.1 / discCount;
			}
			return 0.0;
		}
		
		/// <summary>
		/// Эвристический подсчёт-оценка состояния поля для выбора следующего хода
		/// </summary>
		/// <param name="depth">Текущая глубина поиска</param>
		/// <param name="gameHasWinner">Имеет ли текущее состояние поля победителя</param>
		/// <param name="maxPlayer">Тип игрока</param>
		/// <returns>Значение эвристической оценки</returns>
		private double EvaluateGameState(int depth, bool gameHasWinner, bool maxPlayer)
		{
			var depthCoefficient = MaxDepth - depth + 1;

			if (gameHasWinner)
			{
				return (gameBoard.WinnerIsPlayer()
					? LoseOutcome
					: WinOutcome) / depthCoefficient;
			}

			var score = 0.0;

			var gameBoardChecker = new BoardLineChecker(gameBoard);

			score += gameBoardChecker.CountJoint3(maxPlayer);
			score += gameBoardChecker.CountDisjoint3(maxPlayer) * 0.2;

			if (Math.Abs(score) < Tolerance) return 0.0;

			return (1.0 - 1.0 / score) / depthCoefficient;
		}
	}
}
