﻿namespace ConnectFourAI
{
	public enum Disc
	{
		Empty = 0,
		Player,
		Ai
	}

	public enum GameState
	{
		PlayerWon,
		AiWon,
		Tie
	}

	public class Board
	{
		public GameState GameState;

		public readonly int Height;
		public readonly int Width;
		public readonly int WinLength;

		/// <summary>
		/// Массив фишек (в роли которого играет двумерный массив)
		/// </summary>
		private readonly Disc[,] field;

		public Disc[,] GameField
		{
			get { return field; }
		}

		public Board(int height, int width, int lengthToWin)
		{
			Height = height;
			Width = width;
			WinLength = lengthToWin;
			field = new Disc[width, height];
		}

		public override string ToString()
		{
			var output = string.Empty;
			for (var width = 0; width < Width; width++)
			{
				output += width + " ";
			}
			output += "\n";
			for (var height = Height - 1; height >= 0; height--)
			{
				for (var width = 0; width < Width; width++)
				{
					output += field[width, height] == Disc.Empty 
						? ". " 
						: (field[width, height] == Disc.Ai 
							? "A " 
							: "P ");
				}
				output += "\n";
			}

			return output;
		}

		/// <summary>
		/// Возвращает true, если игровая доска полностью заполнена, иначе - false.
		/// </summary>
		public bool FullBoard
		{
			get
			{
				for (var column = 0; column < Width; column++)
				{
					if (HasEmptyCells(column))
						return false;
				}
				return true;
			}
		}

		public double DiscCount
		{
			get
			{
				var sum = 0;
				for (var column = 0; column < Width; column++)
				{
					sum += ColumnDiscCount(column) + 1;
				}
				return sum;
			}
		}

		/// <summary>
		/// Проверяет, если передаваемое значение столбца находилась в границах игрового поля
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если номер столбца находится в границах игрового поля</returns>
		private bool InsideSideBoards(int column)
		{
			return column >= 0 && column < Width;
		}

		/// <summary>
		/// Проверяет, если данный столбец имеет пустые клетки
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если данный столбец имеет пустые клетки</returns>
		private bool HasEmptyCells(int column)
		{
			return field[column, Height - 1] == Disc.Empty;
		}

		/// <summary>
		/// Проверяет, что ход в данный столбец возможен
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если возможно сделать ход в данный столбец</returns>
		public bool IsValidMove(int column)
		{
			return InsideSideBoards(column) && HasEmptyCells(column);
		}

		/// <summary>
		/// Делает ход для игрока в данный столбец
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был выполнен</returns>
		public bool MakePlayerMove(int column)
		{
			if (!IsValidMove(column)) return false;

			for (var position = 0; position < Height; position++)
			{
				if (field[column, position] != Disc.Empty) continue;
				field[column, position] = Disc.Player;
				break;
			}

			return true;
		}

		/// <summary>
		/// Выполняет ход ИИ
		/// </summary>
		/// <param name="column">Номпер столбца</param>
		/// <returns>true - если ход был выполнен</returns>
		public bool MakeAiMove(int column)
		{
			if (!IsValidMove(column)) return false;

			for (var position = 0; position < Height; position++)
			{
				if (field[column, position] != Disc.Empty) continue;
				field[column, position] = Disc.Ai;
				break;
			}

			return true;
		}

		/// <summary>
		/// Возвращает количество занятых ячеек столбца
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>Количество занятых ячеек столбца</returns>
		private int ColumnDiscCount(int column)
		{
			int columnDiscCount;
			for (columnDiscCount = Height - 1; columnDiscCount >= 0; columnDiscCount--)
			{
				if (field[column, columnDiscCount] != Disc.Empty) break;
			}
			return columnDiscCount;
		}

		/// <summary>
		/// Отменяет ход игрока
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был отменён</returns>
		public bool UndoPlayerMove(int column)
		{
			var columnDiscCount = ColumnDiscCount(column);

			if (columnDiscCount < 0) return false;

			if (!InsideSideBoards(column) || field[column, columnDiscCount] != Disc.Player) return false;

			field[column, columnDiscCount] = Disc.Empty;
			return true;
		}

		/// <summary>
		/// Отменяет ход ИИ
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был отменён</returns>
		public bool UndoAiMove(int column)
		{
			var columnDiscCount = ColumnDiscCount(column);

			if (columnDiscCount < 0) return false;

			if (!InsideSideBoards(column) || field[column, columnDiscCount] != Disc.Ai) return false;

			field[column, columnDiscCount] = Disc.Empty;
			return true;
		}

		public bool WinnerIsPlayer()
		{
			return GameState == GameState.PlayerWon;
		}

		public bool IsTie()
		{
			return GameState == GameState.Tie;
		}

		public int GetFirstEmptyAt(int column)
		{
			return Height - ColumnDiscCount(column) - 1;
		}
	}
}
