using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectFourAI
{
	public class BoardLineChecker
	{
		private readonly Board board;

		public BoardLineChecker(Board board)
		{
			this.board = board;
		}

		/// <summary>
		///     ���������, ��� �� ������� ����� ���� ����� ����������. ��������� ����� ������� � gameState
		/// </summary>
		/// <returns>true - ���� ���� ����������</returns>
		public bool HasWinner()
		{
			var state = CheckColumns();
			if (state != null)
			{
				board.GameState = state.Value;
				return true;
			}

			state = CheckRows();
			if (state != null)
			{
				board.GameState = state.Value;
				return true;
			}

			state = CheckDiagonals();
			if (state != null)
			{
				board.GameState = state.Value;
				return true;
			}

			if (!board.FullBoard) return false;

			board.GameState = GameState.Tie;
			return false;
		}

		private GameState? CheckDiagonals()
		{

			var player = Enumerable.Repeat(Disc.Player, board.WinLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, board.WinLength).ToArray();

			for (var column = 0; column <= board.Width - board.WinLength; column++)
			{
				for (var row = 0; row <= board.Height - board.WinLength; row++)
				{
					var subDiagonalUpRight = GetSubDiagonal(board.GameField, column, column + board.WinLength, row, true);
					var subDiagonalDownRight = GetSubDiagonal(board.GameField, column, column + board.WinLength, row + board.WinLength - 1, false);
					if (subDiagonalUpRight != null)
					{
						var isPlayer = subDiagonalUpRight.SequenceEqual(player);
						if (isPlayer)
							return GameState.PlayerWon;
						var isAi = subDiagonalUpRight.SequenceEqual(ai);
						if (isAi)
							return GameState.AiWon;
					}
					if (subDiagonalDownRight != null)
					{
						var isPlayer = subDiagonalDownRight.SequenceEqual(player);
						if (isPlayer)
							return GameState.PlayerWon;
						var isAi = subDiagonalDownRight.SequenceEqual(ai);
						if (isAi)
							return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private GameState? CheckRows()
		{
			var player = Enumerable.Repeat(Disc.Player, board.WinLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, board.WinLength).ToArray();

			for (var row = 0; row < board.Height; row++)
			{
				for (var pos = 0; pos <= board.Width - board.WinLength; pos++)
				{
					var subRow = GetSubRow(board.GameField, row, pos, pos + board.WinLength);
					if (subRow == null) continue;
					var isPlayer = subRow.SequenceEqual(player);
					if (isPlayer)
					{
						return GameState.PlayerWon;
					}
					var isAi = subRow.SequenceEqual(ai);
					if (isAi)
					{
						return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private GameState? CheckColumns()
		{
			var player = Enumerable.Repeat(Disc.Player, board.WinLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, board.WinLength).ToArray();

			for (var column = 0; column < board.Width; column++)
			{
				for (var pos = 0; pos <= board.Height - board.WinLength; pos++)
				{
					var subColumn = GetSubColumn(board.GameField, column, pos, pos + board.WinLength);
					if (subColumn == null) continue;
					var isPlayer = subColumn.SequenceEqual(player);
					if (isPlayer)
					{
						return GameState.PlayerWon;
					}
					var isAi = subColumn.SequenceEqual(ai);
					if (isAi)
					{
						return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private static Disc[] GetSubColumn(Disc[,] array, int column, int posFirst, int posLast)
		{
			var result = new Disc[posLast - posFirst];
			for (var pos = posFirst; pos < posLast; pos++)
			{
				//if (array[column, pos] == Disc.Empty) return null;
				result[pos - posFirst] = array[column, pos];
			}
			return result;
		}

		private static Disc[] GetSubRow(Disc[,] array, int row, int posFirst, int posLast)
		{
			var result = new Disc[posLast - posFirst];
			for (var pos = posFirst; pos < posLast; pos++)
			{
				//if (array[pos, row] == Disc.Empty) return null;
				result[pos - posFirst] = array[pos, row];
			}
			return result;
		}

		private static Disc[] GetSubDiagonal(Disc[,] array, int leftX, int rightX, int y, bool upright)
		{
			var result = new Disc[rightX - leftX];
			for (var pos = leftX; pos < rightX; pos++)
			{
				var disc = array[pos, upright ? y++ : y--];
				//if (disc == Disc.Empty) return null;
				result[pos - leftX] = disc;
			}
			return result;
		}


		private int CountDiagonals(int length, List<Disc> sequence)
		{
			if (length > Math.Min(board.Width, board.Height))
			{
				throw new ArgumentException("length parameter must be lesser than min of board width and height");
			}

			var sum = 0;

			for (var column = 0; column <= board.Width - length; column++)
			{
				for (var row = 0; row <= board.Height - length; row++)
				{
					var subDiagonalUpRight = GetSubDiagonal(board.GameField, column, column + length, row, true);
					var subDiagonalDownRight = GetSubDiagonal(board.GameField, column, column + length, row + length - 1, false);
					if (subDiagonalUpRight != null)
					{
						if (subDiagonalUpRight.SequenceEqual(sequence))
							++sum;
					}
					if (subDiagonalDownRight != null)
					{
						if (subDiagonalDownRight.SequenceEqual(sequence))
							++sum;
					}
				}
			}
			return sum;
		}

		private int CountRows(int length, List<Disc> sequence)
		{
			if (length > Math.Min(board.Width, board.Height))
			{
				throw new ArgumentException("length parameter must be lesser than min of board width and height");
			}

			var sum = 0;

			for (var row = 0; row < board.Height; row++)
			{
				for (var pos = 0; pos <= board.Width - length; pos++)
				{
					var subRow = GetSubRow(board.GameField, row, pos, pos + length);
					if (subRow == null) continue;
					if (subRow.SequenceEqual(sequence))
					{
						++sum;
					}
				}
			}
			return sum;
		}

		private int CountColumns(int length, List<Disc> sequence)
		{
			if (length > board.Height)
			{
				throw new ArgumentException("length parameter must be lesser than min of board width and height");
			}

			var sum = 0;

			for (var column = 0; column < board.Width; column++)
			{
				for (var pos = 0; pos <= board.Height - length; pos++)
				{
					var subColumn = GetSubColumn(board.GameField, column, pos, pos + length);
					if (subColumn == null) continue;
					if (subColumn.SequenceEqual(sequence))
					{
						++sum;
					}
				}
			}
			return sum;
		}


		public double CountJoint3(bool maxPlayer)
		{
			var player = maxPlayer ? Disc.Ai : Disc.Player;

			var sequence = Enumerable.Repeat(player, 3).ToList();
			var sequence1 = sequence.Concat(new[] {Disc.Empty}).ToList();
			var sequence2 = new[] {Disc.Empty}.Concat(sequence).ToList();
			var sequence3 = new[] {Disc.Empty}.Concat(sequence1).ToList();


			var sum1 = CountColumns(4, sequence1) + CountRows(4, sequence1) + CountDiagonals(4, sequence1);
			var sum2 = CountColumns(4, sequence2) + CountRows(4, sequence2) + CountDiagonals(4, sequence2);
			var sum3 = CountColumns(5, sequence3) + CountRows(5, sequence3) + CountDiagonals(5, sequence3);


			return sum1 + sum2 + sum3;
		}

		public double CountDisjoint3(bool maxPlayer)
		{
			var player = maxPlayer ? Disc.Ai : Disc.Player;

			var sequence1 = Enumerable.Repeat(player, 3).ToList();
			sequence1.Insert(1, Disc.Empty);
			var sequence2 = sequence1;
			sequence2.Reverse();


			var sum1 = CountColumns(4, sequence1) + CountRows(4, sequence1) + CountDiagonals(4, sequence1);
			var sum2 = CountColumns(4, sequence2) + CountRows(4, sequence2) + CountDiagonals(4, sequence2);


			return sum1 + sum2;
		}
	}
}