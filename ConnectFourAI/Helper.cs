﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFourAI
{
	public static class Helper
	{
		public static List<int> GetRange(int a, int b)
		{
			return Enumerable.Range(a, b).ToList();
		}

		public static void Shuffle<T>(this List<T> list)
		{
			var random = new Random();
			var n = list.Count;
			while (n > 1)
			{
				n--;
				var k = random.Next(n + 1);
				var value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}
	}
}
