﻿using System;

namespace ConnectFourAI
{
	class AiPlayer
	{
		/// <summary>
		/// Шаги, просчитываемые ИИ вперёд.
		/// </summary>
		private const int MaxDepth = 7;

		public const double Tolerance = 0.00000001;

		/// <summary>
		/// Очки, выдаваемые состоянию, следующему за текущим.
		/// </summary>
		private const double WinOutcome = 1d;
		private const double UncertainOutcome = 0d;
		private const double LoseOutcome = -1d;



		private readonly Board gameBoard;

		public AiPlayer(Board currentGameBoard)
		{
			gameBoard = currentGameBoard;
		}

		/// <summary>
		/// Выполняет ход 
		/// </summary>
		/// <returns>Номер колонки поля, куда был выполнен ход</returns>
		public int MakeTurn()
		{
			var maxValue = double.MinValue;
			var move = 0;

			for (var column = 0; column < gameBoard.Width; column++)
			{
				if (!gameBoard.IsValidMove(column)) continue;
				
				var value = MoveValue(column);
				
				if (!(value > maxValue)) continue;
				
				maxValue = value;
				move = column;
				if (Math.Abs(value - WinOutcome) < Tolerance)
					break;
			}

			gameBoard.MakeAiMove(move);
			return move;
		}

		/// <summary>
		/// Вычисление значимости хода в столбец
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>Значимость хода</returns>
		public double MoveValue(int column)
		{
			gameBoard.MakeAiMove(column);
			var value = AlphaBeta(MaxDepth, double.MinValue, double.MaxValue, false);
			gameBoard.UndoAiMove(column);
			return value;
		}

		/// <summary>
		/// Альфа-бета отсечение
		/// </summary>
		/// <param name="depth">Уровни глубины, которые необходимо проверить</param>
		/// <param name="alpha">Параметр альфа</param>
		/// <param name="beta">Параметр бета</param>
		/// <param name="maxPlayer">тип игрока (true - Max, false - Min)</param>
		/// <returns>Значимость хода</returns>
		public double AlphaBeta(int depth, double alpha, double beta, bool maxPlayer)
		{
			var gameHasWinner = gameBoard.HasWinner();

			if (depth == 0 || gameHasWinner)
			{
				double score;
				if (gameHasWinner)
				{
					score = gameBoard.WinnerIsPlayer()
						? LoseOutcome
						: WinOutcome;
				}
				else
				{
					score = UncertainOutcome;
				}

				return score/(MaxDepth - depth + 1);
			}

			if (maxPlayer)
			{
				for (var column = 0; column < gameBoard.Width; column++)
				{
					if (!gameBoard.IsValidMove(column)) continue;

					gameBoard.MakeAiMove(column);
					alpha = Math.Max(
						alpha,
						AlphaBeta(
							depth: depth - 1, 
							alpha: alpha, 
							beta: beta, 
							maxPlayer: false));
					gameBoard.UndoAiMove(column);
					if (beta <= alpha)
						break;
				}
				return alpha;
			}
			else
			{
				for (var column = 0; column < gameBoard.Width; column++)
				{
					if (!gameBoard.IsValidMove(column)) continue;

					gameBoard.MakePlayerMove(column);
					beta = Math.Min(
						beta,
						AlphaBeta(
							depth: depth - 1, 
							alpha: alpha, 
							beta: beta, 
							maxPlayer: true));
					gameBoard.UndoPlayerMove(column);
					if (beta <= alpha)
						break;
				}
				return beta;
			}
		}
	}
}
