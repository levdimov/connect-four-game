﻿using System;
using System.Linq;

namespace ConnectFourAI
{
	class Board
	{
		private enum Disc
		{
			Empty = 0,
			Player,
			Ai
		}

		private enum GameState
		{
			PlayerWon,
			AiWon,
			Tie
		}

		private GameState gameState;

		public readonly int Height;
		public readonly int Width;
		private readonly int winLength;

		/// <summary>
		/// Массив столбцов фишек (в роли которого играет контейнер List)
		/// </summary>
		private readonly Disc[,] board;

		public Board(int height, int width, int lengthToWin)
		{
			Height = height;
			Width = width;
			winLength = lengthToWin;
			board = new Disc[width, height];
		}

		public override string ToString()
		{
			var output = string.Empty;
			for (var width = 0; width < Width; width++)
			{
				output += width + " ";
			}
			output += "\n";
			for (var height = Height - 1; height >= 0; height--)
			{
				for (var width = 0; width < Width; width++)
				{
					output += board[width, height] == Disc.Empty 
						? ". " 
						: (board[width, height] == Disc.Ai 
							? "A " 
							: "P ");
				}
				output += "\n";
			}

			return output;
		}

		/// <summary>
		/// Возвращает true, если игровая доска полностью заполнена, иначе - false.
		/// </summary>
		private bool FullBoard
		{
			get
			{
				var hasEmptyColumn = false;
				for (var column = 0; column < Width; column++)
				{
					if (!HasEmptyCells(column)) continue;

					hasEmptyColumn = true;
					break;
				}
				return !hasEmptyColumn;
			}
		}

		/// <summary>
		/// Проверяет, если передаваемое значение столбца находилась в границах игрового поля
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если номер столбца находится в границах игрового поля</returns>
		private bool InsideSideBoards(int column)
		{
			return column >= 0 && column < Width;
		}

		/// <summary>
		/// Проверяет, если данный столбец имеет пустые клетки
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если данный столбец имеет пустые клетки</returns>
		private bool HasEmptyCells(int column)
		{
			return board[column, Height - 1] == Disc.Empty;
		}

		/// <summary>
		/// Проверяет, что ход в данный столбец возможен
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если возможно сделать ход в данный столбец</returns>
		public bool IsValidMove(int column)
		{
			return InsideSideBoards(column) && HasEmptyCells(column);
		}

		/// <summary>
		/// Делает ход для игрока в данный столбец
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был выполнен</returns>
		public bool MakePlayerMove(int column)
		{
			if (!IsValidMove(column)) return false;

			for (var position = 0; position < Height; position++)
			{
				if (board[column, position] != Disc.Empty) continue;
				board[column, position] = Disc.Player;
				break;
			}

			return true;
		}

		/// <summary>
		/// Выполняет ход ИИ
		/// </summary>
		/// <param name="column">Номпер столбца</param>
		/// <returns>true - если ход был выполнен</returns>
		public bool MakeAiMove(int column)
		{
			if (!IsValidMove(column)) return false;

			for (var position = 0; position < Height; position++)
			{
				if (board[column, position] != Disc.Empty) continue;
				board[column, position] = Disc.Ai;
				break;
			}

			return true;
		}

		/// <summary>
		/// Возвращает количество занятых ячеек столбца
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>Количество занятых ячеек столбца</returns>
		private int ColumnDiscCount(int column)
		{
			int columnDiscCount;
			for (columnDiscCount = Height - 1; columnDiscCount >= 0; columnDiscCount--)
			{
				if (board[column, columnDiscCount] != Disc.Empty) break;
			}
			return columnDiscCount;
		}

		/// <summary>
		/// Отменяет ход игрока
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был отменён</returns>
		public bool UndoPlayerMove(int column)
		{
			var columnDiscCount = ColumnDiscCount(column);

			if (columnDiscCount < 0) return false;

			if (!InsideSideBoards(column) || board[column, columnDiscCount] != Disc.Player) return false;

			board[column, columnDiscCount] = Disc.Empty;
			return true;
		}

		/// <summary>
		/// Отменяет ход ИИ
		/// </summary>
		/// <param name="column">Номер столбца</param>
		/// <returns>true - если ход был отменён</returns>
		public bool UndoAiMove(int column)
		{
			var columnDiscCount = ColumnDiscCount(column);

			if (columnDiscCount < 0) return false;

			if (!InsideSideBoards(column) || board[column, columnDiscCount] != Disc.Ai) return false;

			board[column, columnDiscCount] = Disc.Empty;
			return true;
		}

		/// <summary>
		/// Проверяет, что на текущем этапе игра имеет победителя. Результат будет записан в gameState 
		/// </summary>
		/// <returns>true - если есть победитель</returns>
		public bool HasWinner()
		{
			var state = CheckColumns();
			if (state != null)
			{
				gameState = state.Value;
				return true;
			}

			state = CheckRows();
			if (state != null)
			{
				gameState = state.Value;
				return true;
			}

			state = CheckDiagonals();
			if (state != null)
			{
				gameState = state.Value;
				return true;
			}

			if (!FullBoard) return false;

			gameState = GameState.Tie;
			return false;
		}

		private GameState? CheckDiagonals()
		{
			var player = Enumerable.Repeat(Disc.Player, winLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, winLength).ToArray();

			for (var column = 0; column < Width - winLength; column++)
			{
				for (var row = 0; row < Height - winLength; row++)
				{
					var subDiagonalUpRight = GetSubDiagonal(board, column, column + winLength, row, true);
					var subDiagonalDownRight = GetSubDiagonal(board, column, column + winLength, row + winLength - 1, false);
					if (subDiagonalUpRight != null)
					{
						var isPlayer = subDiagonalUpRight.SequenceEqual(player);
						if (isPlayer)
							return GameState.PlayerWon;
						var isAi = subDiagonalUpRight.SequenceEqual(ai);
						if (isAi)
							return GameState.AiWon;
					}
					if (subDiagonalDownRight != null)
					{
						var isPlayer = subDiagonalDownRight.SequenceEqual(player);
						if (isPlayer)
							return GameState.PlayerWon;
						var isAi = subDiagonalDownRight.SequenceEqual(ai);
						if (isAi)
							return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private GameState? CheckRows()
		{
			var player = Enumerable.Repeat(Disc.Player, winLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, winLength).ToArray();

			for (var row = 0; row < Height; row++)
			{
				for (var pos = 0; pos < Width - winLength; pos++)
				{
					var subRow = GetSubRow(board, row, pos, pos + winLength);
					if (subRow == null) continue;
					var isPlayer = subRow.SequenceEqual(player);
					if (isPlayer)
					{
						return GameState.PlayerWon;
					}
					var isAi = subRow.SequenceEqual(ai);
					if (isAi)
					{
						return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private GameState? CheckColumns()
		{
			var player = Enumerable.Repeat(Disc.Player, winLength).ToArray();
			var ai = Enumerable.Repeat(Disc.Ai, winLength).ToArray();

			for (var column = 0; column < Width; column++)
			{
				for (var pos = 0; pos < Height - winLength; pos++)
				{
					var subColumn = GetSubColumn(board, column, pos, pos + winLength);
					if (subColumn == null) continue;
					var isPlayer = subColumn.SequenceEqual(player);
					if (isPlayer)
					{
						return GameState.PlayerWon;
					} 
					var isAi = subColumn.SequenceEqual(ai);
					if (isAi)
					{
						return GameState.AiWon;
					}
				}
			}
			return null;
		}

		private static Disc[] GetSubColumn(Disc[,] array, int column, int posFirst, int posLast)
		{
			var result = new Disc[posLast - posFirst];
			for (var pos = posFirst; pos < posLast; pos++)
			{
				if (array[column, pos] == Disc.Empty) return null;
				result[pos - posFirst] = array[column, pos];
			}
			return result;
		}

		private static Disc[] GetSubRow(Disc[,] array, int row, int posFirst, int posLast)
		{
			var result = new Disc[posLast - posFirst];
			for (var pos = posFirst; pos < posLast; pos++)
			{
				if (array[pos, row] == Disc.Empty) return null;
				result[pos - posFirst] = array[pos, row];
			}
			return result;
		}

		private static Disc[] GetSubDiagonal(Disc[,] array, int leftX, int rightX, int y, bool upright)
		{
			var result = new Disc[rightX - leftX];
			for (var pos = leftX; pos < rightX; pos++)
			{
				var disc = array[pos, upright ? y++ : y--];
				if (disc == Disc.Empty) return null;
				result[pos - leftX] = disc;
			}
			return result;
		}

		public bool WinnerIsPlayer()
		{
			return gameState == GameState.PlayerWon;
		}

		public bool IsTie()
		{
			return gameState == GameState.Tie;
		}
	}
}
