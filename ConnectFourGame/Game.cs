﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using ConnectFourAI;

namespace ConnectFourGame
{
	public class Game
	{
		private readonly MainWindow mainGameWindow;
		private readonly Log log;

		public Board GameBoard;
		public AiPlayer Ai;

		public Game(MainWindow mainWindow, Log gameLog)
		{
			mainGameWindow = mainWindow;
			log = gameLog;
		}


		public void InitializeGame()
		{
			MainViewModel.Instance.SetDefaultGameParameters();

			InitializeField();

			GameBoard = new Board(MainViewModel.Instance.GameFieldHeight, MainViewModel.Instance.GameFieldWidth, MainViewModel.Instance.GameWinLength);
			Ai = new AiPlayer(GameBoard);

			var random = new Random();
			var probability = random.NextDouble();
			MainViewModel.Instance.IsAiTurn = probability < 0.5;

			if (MainViewModel.Instance.IsAiTurn) MakeAiTurn();
			// else wait for user action
		}

		private void InitializeField()
		{
			int width = MainViewModel.Instance.GameFieldWidth,
				height = MainViewModel.Instance.GameFieldHeight;

			for (var column = 0; column < width; column++)
			{
				mainGameWindow.GameFieldGrid.ColumnDefinitions.Add(new ColumnDefinition
				{
					Width = new GridLength(1, GridUnitType.Star)
				});
			}

			for (var column = 0; column < width; column++)
			{
				var backRectangle = new Canvas
				{
					Background = Brushes.DarkGray
				};

				backRectangle.MouseEnter += delegate(object sender, MouseEventArgs args)
				{
					var rectangle = sender as Canvas;
					if (rectangle != null) rectangle.Background = Brushes.LightGray;
				};
				backRectangle.MouseLeave += delegate(object sender, MouseEventArgs args)
				{
					var rectangle = sender as Canvas;
					if (rectangle != null) rectangle.Background = Brushes.DarkGray;
				};

				backRectangle.MouseLeftButtonDown += GameFieldClick;

				mainGameWindow.GameFieldGrid.Children.Add(backRectangle);

				for (var row = 0; row < height; row++)
				{
					var panel = mainGameWindow.GameFieldGrid.Children[column] as Canvas;

					var side = mainGameWindow.GameFieldGrid.ActualWidth / width;
					var discDiameter = side - side / 5.0;

					var disc = new Ellipse
					{
						Fill = Brushes.WhiteSmoke,
						Height = discDiameter - 1,
						Width = discDiameter - 1,
						Margin = new Thickness(side / 10.0 + 0.5, 0, side / 10.0 + 0.5, side / 10.0 + 0.5)
					};
					if (panel != null) panel.Children.Add(disc);
					Canvas.SetBottom(disc, side * row - side / 20.0);
				}


				Grid.SetColumn(backRectangle, column);
			}
		}

		private void GameFieldClick(object sender, MouseButtonEventArgs e)
		{
			var panel = sender as Canvas;
			if (panel == null) return;

			var column = Grid.GetColumn(panel);

			if (!GameBoard.MakePlayerMove(column))
			{
				log.WriteLine("Колонка {0} заполнена.", column);
				Helper.ShowWarning("Сюда нельзя сходить. Выбранная колонка заполнена.");
				return;
			}

			log.WriteLine("Вы поместили диск в колонку {0}", column);
			PutDisc(column, false);
			ContinueGame();
		}

		private void PutDisc(int column, bool isAi)
		{
			var rows = MainViewModel.Instance.GameFieldHeight;

			var insertPos = GameBoard.GetFirstEmptyAt(column);

			var panel = mainGameWindow.GameFieldGrid.Children[column] as Canvas;
			if (panel == null)
			{
				return;
			}

			var side = panel.ActualWidth;
			var discDiameter = side - side / 5.0;

			var disc = new Ellipse
			{
				Fill = isAi ? Brushes.LightCoral : Brushes.LightBlue,
				Height = discDiameter,
				Width = discDiameter,
				Margin = new Thickness(side / 10.0, 0, side / 10.0, side / 10.0)
			};

			panel.Children.Add(disc);
			Canvas.SetBottom(disc, side * (rows - insertPos - 1) - side / 20.0);
		}

		private void MakeAiTurn()
		{
			var bw = new BackgroundWorker();
			var column = 0;

			bw.DoWork += delegate
			{
				column = Ai.MakeTurn();
			};
			bw.RunWorkerCompleted += delegate
			{
				log.WriteLine("ИИ поместил диск в колонку {0}.", column);
				PutDisc(column, true);
				ContinueGame();
			};
			bw.RunWorkerAsync();
		}

		private void ContinueGame()
		{
			MainViewModel.Instance.IsAiTurn = !MainViewModel.Instance.IsAiTurn;

			if (CheckGameState())
			{
				mainGameWindow.TurnLabel.Content = "Игра окончена";
				mainGameWindow.IsEnabled = false;
				mainGameWindow.Restart(true);
			}

			if (MainViewModel.Instance.IsAiTurn)
				MakeAiTurn();
		}

		private bool CheckGameState()
		{
			if (!(new BoardLineChecker(GameBoard)).HasWinner())
			{
				if (!GameBoard.IsTie()) return false;

				log.WriteLine("Ничья.");
				Helper.ShowInformation("Ничья.", "Конец игры");
				return true;
			}

			if (GameBoard.WinnerIsPlayer())
			{
				log.WriteLine("Вы выйграли!");
				Helper.ShowInformation("Вы выйграли!", "Конец игры");
			}
			else
			{
				log.WriteLine("Выйграл ИИ");
				Helper.ShowInformation("Выйграл ИИ.", "Конец игры");
			}

			return true;
		}

	}
}