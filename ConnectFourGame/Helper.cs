﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;

namespace ConnectFourGame
{
	class Helper
	{
		public static MessageBoxResult ShowWarning(
			string warningText, 
			string header = "Предупреждение",
			MessageBoxButton button = MessageBoxButton.OK)
		{
			return MessageBox.Show(
				warningText,
				header,
				button,
				MessageBoxImage.Warning);
		}

		public static MessageBoxResult ShowError(
			string errorText,
			string header = "Ошибка",
			MessageBoxButton button = MessageBoxButton.OK)
		{
			return MessageBox.Show(
				errorText,
				header,
				button,
				MessageBoxImage.Error);
		}

		public static MessageBoxResult ShowInformation(
			string infoText,
			string header = "Информация",
			MessageBoxButton button = MessageBoxButton.OK)
		{
			return MessageBox.Show(
				infoText,
				header,
				button,
				MessageBoxImage.Information);
		}

		public static MessageBoxResult ShowQuestion(
			string infoText,
			string header = "Вопрос",
			MessageBoxButton button = MessageBoxButton.OK)
		{
			return MessageBox.Show(
				infoText,
				header,
				button,
				MessageBoxImage.Question);
		}
	}

	class WindowControlsHelper
	{
		public static IEnumerable<T> GetChildOfType<T>(DependencyObject dependencyObject) where T : DependencyObject
		{
			if (dependencyObject == null) yield break;

			for (var i = 0; i < VisualTreeHelper.GetChildrenCount(dependencyObject); i++)
			{
				var child = VisualTreeHelper.GetChild(dependencyObject, i);
				var childOfType = child as T;
				if (childOfType != null)
				{
					yield return childOfType;
				}

				foreach (var childOfChild in GetChildOfType<T>(child))
				{
					yield return childOfChild;
				}
			}
		}
	}

	class SafeNativeMethods
	{
		/* Removing red 'X' from window */
		// ReSharper disable InconsistentNaming
		private const int GWL_STYLE = -16;
		private const int WS_SYSMENU = 0x80000;

		[DllImport("user32.dll", SetLastError = true)]
		private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

		[DllImport("user32.dll")]
		private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

		public static int RemoveRedX(IntPtr hWnd)
		{
			return SetWindowLong(
				hWnd,
				GWL_STYLE,
				GetWindowLong(hWnd, GWL_STYLE) & ~WS_SYSMENU);
		}
		// ReSharper restore InconsistentNaming
		/*  #####################################################################  */
	}
}
