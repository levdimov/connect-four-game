﻿using System;
using System.Windows.Controls;

namespace ConnectFourGame
{
	public class Log
	{
		private readonly TextBox logBox;

		public Log(TextBox logBox)
		{
			this.logBox = logBox;
		}

		public void WriteLine(string message, params object[] args)
		{
			logBox.AppendText(DateTime.Now.ToLongTimeString() + ": " + string.Format(message, args) + "\n");
			logBox.ScrollToEnd();
		}

		public void Clear()
		{
			logBox.Text = string.Empty;
		}
	}
}
