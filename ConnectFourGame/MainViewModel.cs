﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace ConnectFourGame
{
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public sealed class MainViewModel : INotifyPropertyChanged
	{
		private static readonly MainViewModel instance = new MainViewModel();
		public static MainViewModel Instance { get { return instance; } }

		private MainViewModel()
		{
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyProperyChanged(string propertyName = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		//************************************************************

		private static bool _isAiTurn;
		public bool IsAiTurn
		{
			get { return _isAiTurn; }
			set
			{
				_isAiTurn = value;
				NotifyProperyChanged("IsAiTurn");
			}
		}

		private static int _gameFieldWidth;
		public int GameFieldWidth
		{
			get { return _gameFieldWidth; }
			set
			{
				_gameFieldWidth = value;
				NotifyProperyChanged("GameFieldWidth");
			}
		}

		private static int _gameFieldHeight;
		public int GameFieldHeight
		{
			get { return _gameFieldHeight; }
			set
			{
				_gameFieldHeight = value;
				NotifyProperyChanged("GameFieldHeight");
			}
		}

		private static int _gameWinLength;

		public int GameWinLength
		{
			get { return _gameWinLength; }
			set
			{
				_gameWinLength = value;
				NotifyProperyChanged("GameWinLength");
			}
		}

		public void SetDefaultGameParameters()
		{
			GameFieldHeight = 6;
			GameFieldWidth = 7;
			GameWinLength = 4;
		}
	}
}
