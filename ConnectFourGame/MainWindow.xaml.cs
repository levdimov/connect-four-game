﻿using System;
using System.Diagnostics;
using System.Windows;

namespace ConnectFourGame
{
	public partial class MainWindow
	{
		public Log GameLog;

		private readonly Game game;

		public MainWindow()
		{
			InitializeComponent();
			GameLog = new Log(LogBox);
			game = new Game(this, GameLog);
		}

		private void Window_Loaded(object sender, EventArgs e)
		{
			game.InitializeGame();
		}

		private void RestartGameButton_Click(object sender, RoutedEventArgs e)
		{
			Restart(false);
		}

		public void Restart(bool shouldCloseOnNo)
		{
			var result = Helper.ShowQuestion("Перезапустить игру?", "Начать заново", MessageBoxButton.YesNo);
			if (result == MessageBoxResult.Yes)
			{
				Process.Start(Application.ResourceAssembly.Location);
				Environment.Exit(0);
			}
			if (shouldCloseOnNo)
			{
				Environment.Exit(0);
			}
		}
	}
}
