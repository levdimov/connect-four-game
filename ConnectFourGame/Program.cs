﻿using System;

namespace ConnectFourAI
{
	class Program
	{
		static void Main()
		{
			var random = new Random();
			var probability = random.NextDouble();
			var turn = probability < 0.5;

			var board = new Board(
				height: 6, 
				width: 7, 
				lengthToWin: 4);
			var aiPlayer = new AiPlayer(board);

			while (!board.HasWinner() && !board.IsTie())
			{
				int column;
				Console.WriteLine("\n" + board);
				if (turn)
				{
					string read;
					do
					{
						read = Console.ReadLine();
					} while (string.IsNullOrEmpty(read));
					column = int.Parse(read);
					while (true)
					{
						var result = board.MakePlayerMove(column);
						if (!result)
							Console.WriteLine("Wrong move: column number is wrong or column is full of discs...");
						else break;
					}
					
				}
				else
				{
					column = aiPlayer.MakeTurn();
					Console.WriteLine("Ai placed in column {0}.", column);
				}
				turn = !turn;
			}

			Console.WriteLine("\n" + board);
			if (board.WinnerIsPlayer())
			{
				Console.WriteLine("Winner: player.");
			}
			else if (board.IsTie())
			{
				Console.WriteLine("Tie.");
			}
			else
			{
				Console.WriteLine("Winner: ai");
			}
			Console.ReadKey();
		}
	}
}
